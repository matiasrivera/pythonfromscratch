'''Escribir un programa que simule un carrito de compras. El programa debe permitir, dada una coleccion de items elegidos por
 un cliente y pre-configurados con precio, descuento y % de impuestos a aplicarle, calcular el total a abonar por un cliente, discriminando

a) Monto Neto, monto descuento, monto impuestos, monto bruto item x item
b) Monto Neto, monto descuento, monto impuestos, monto bruto Total del la compra
'''


import random

productos = ["Leche","Pan","Chocolate","Vino","Manteca","Cerveza"]

almacen = {"A"+str(indice):{"nombre":producto
    ,"precio":random.randint(50*(indice+1),400)
    ,"descuento":random.randint(10,20)/100
    ,"impuesto":random.randint(10,21)/100} for indice,producto in enumerate(productos)}


print(almacen)

carrito = [(producto,random.randint(1,6)) for producto in almacen if almacen[producto]["precio"] < 300]

carrito2 = [{"producto":producto,"cantidad":random.randint(1,6)} for producto in almacen if almacen[producto]["precio"] < 300]


print(carrito)
print(carrito2)

factura = []

for item in carrito2:
    prod = almacen[item["producto"]]
    cantidad = item["cantidad"]
    #prod = almacen[item[0]]
    #cantidad = item[1]
    neto = prod["precio"]*cantidad
    descuento = prod["descuento"]*neto
    impuesto = prod["impuesto"]*neto
    subtotal = neto-descuento+impuesto
    factura.append({"nombre":prod["nombre"],"cantidad":cantidad,"neto":neto,"descuento":descuento,"impuesto":impuesto,"subtotal":subtotal})

print(factura)

print(f"{'Producto':10}  {'Cantidad'}     {'Neto':8}  {'Descuento'}   {'Impuesto'}    {'Subtotal'}")
total = 0
for renglon in factura:
    total+=renglon['subtotal']
    print(f"{renglon['nombre']:10}  {renglon['cantidad']:7}     ${renglon['neto']:>8.2f}  ${renglon['descuento']:>6.2f}     ${renglon['impuesto']:>6.2f}  ${renglon['subtotal']:>8.2f}")

print(f"{'Total':*>52}    ${total:8.2f}")