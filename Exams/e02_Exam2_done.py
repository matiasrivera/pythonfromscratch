'''
Ejercicios  a   resolver
Se  les  va  a   proveer  de  un  archivo  de  texto  con  campos  separados  con  comas  que  va  a
contener  el  book  de  órdenes  de  un  día  de  un  mercado.  A   partir  de  esta  información  deben
generar  las  siguientes  salidas.

1. Escribir  en  un  nuevo  archivo  todos  los  trades  que  se  realizaron  (   es  decir  cuando
dos  órdenes  matchearian,  uno  por  línea),   conteniendo  la  siguiente  información.
Cuenta  compra,  Cuenta  venta,  precio,  tamaño,  instrumento  y   hora.
2. Calcular  el  volumen  operado  por  instrumento  y   el  volumen  total  del  mercado  de
ese  día  e   imprimirlos  por  pantalla  al  finalizar  la  ejecución  del  programa.
3. Suponiendo  un  estado  inicial  en  el  cual  cada  cuenta  comienza  en  0,  calcular
para  cada  una  de  ellas  el  resultado  final  del  día,  y   guardarlo  en  un  archivo
separado  para  cada  una.  Los  datos  a   guardar  serian:  Dinero  efectivo,  y   cantidad
de  acciones/bonos/futuros  de  cada  tipo  a   recibir  y/o  entregar.
'''

import logging
from datetime import datetime

class Order:
    BUY = 0
    SELL = 1
    FILLED = "FILLED"
    PARTIALLY_FILLED = "PARTIALLY_FILLED"
    REJECTED = "REJECTED"
    NEW = "NEW"

    def __init__(self,id,server_ts,account,side,price,order_type,time_in_force,symbol,size):
        self.id = id
        self.timestamp = int(server_ts)
        self.account = account
        self.side = int(side)
        self.price = float(price)
        self.order_type = order_type
        self.time_in_force = time_in_force
        self.symbol = symbol
        self.size = float(size)
        self.status = Order.NEW
        self._account = None

    def getSide(self):
        return "SELL" if self.side == Order.SELL else "BUY"

    def updateSize(self,size):
        self.size -= size
        self.status = Order.FILLED if self.size == 0 else Order.PARTIALLY_FILLED

    def getPositionSize(self):
        return self.size if self.side == Order.BUY else -self.size

    def __str__(self):
        return f"{self.id} {self.getSide()} {self.price} {self.size} {self.account}"


class MatchingEngine:

    def __init__(self):
        self._sellBook = Book(Order.SELL)
        self._buyBook = Book(Order.BUY)
        self._trades = []
        self._accounts = {}

    def addOrder(self,order):
        if not order.account in self._accounts:
            self._accounts[order.account] = Account(order.account)
        self._sellBook.addOrder(order) if order.side == Order.SELL else self._buyBook.addOrder(order)
        if self._sellBook.newTopOfBook() or self._buyBook.newTopOfBook():
            self._match()

    def _match(self):
        bb = self._buyBook.getTopOfBook()
        ba = self._sellBook.getTopOfBook()
        while bb and ba and bb.price >= ba.price:
            if bb.account != ba.account:
                logging.info(f"Orders {bb} and {ba} match!")
                bb._account = self._accounts[bb.account]
                ba._account = self._accounts[ba.account]
                self._trades.append(Trade(bb,ba))
                #debo quitar las ordenes que hayan quedado con size 0
                if bb.status == Order.FILLED:
                    self._buyBook.removeTopOfBook()
                    bb = self._buyBook.getTopOfBook()
                if ba.status == Order.FILLED:
                    self._sellBook.removeTopOfBook()
                    ba = self._sellBook.getTopOfBook()
            else:
                logging.info(f"Orders {bb} and {ba} same account! REJECTING")
                lastOrder = bb if bb.timestamp < ba.timestamp else ba
                lastOrder.status = Order.REJECTED
                self._buyBook.removeTopOfBook() if lastOrder.side == Order.BUY else self._sellBook.removeTopOfBook()
                if lastOrder == ba:
                    ba = None
                else:
                    bb = None

    def showTrades(self):
        totalVolume = 0
        for trade in self._trades:
            totalVolume += trade.size
            logging.info(f"{trade}")
        logging.info(f"Total Volume {totalVolume}")

    def showProfit(self):
        for account in self._accounts.values():
            lasttrade = self._trades[-1].price
            dif = lasttrade - account.price
            profit = dif * account.position * 1000
            account.profit += profit
            logging.info(f"{account} ppc {account.price}")

    def plotTrades(self):
        import matplotlib.pyplot as plt
        import datetime as dt
        #fig,ax = plt.subplot()
        prices = [trade.price for trade in self._trades]
        timestamp = [dt.datetime.strftime(datetime.fromtimestamp(trade.timestamp/1000),"%d/%m/%Y %H:%M:%S") for trade in self._trades]
        plt.plot(timestamp,prices)
        plt.show()

class Account:

    def __init__(self,account):
        self.account = account
        self.position = 0
        self.profit = 0
        self.price = 0

    def updatePosition(self,trade,side):
        s = self.position
        size = trade.size if side == Order.BUY else -trade.size
        p = self.price
        price = trade.price
        #if (s>0 and s+size <0) or (s<0 and s+size>0):
        #    self.profit = round((p-price)*size,3)*1000
        #    logging.info(f"Posición cerrada en cuenta {self.account} {trade.symbol} con profit ${self.profit}")
        #voy a chequear si aumento o cierro posición
        if abs(s+size) > abs(s):
            #si aumento posición, voy a calcular el nuevo precio
            self.price = round((p*s+size*price)/(s+size),3)
            self.position += size
        else:
            #si disminuyo posición voy a calcular el profit
            #primero me fijo si estoy vendido o comprado
            self.profit = round((p-price)*size,3)*1000
            logging.info(f"Posición cerrada en cuenta {self.account} {order.symbol} con profit ${self.profit}")

    def __str__(self):
        return f"Acccount {self.account} profit ${self.profit:.2f} position {self.position}"

class Book:

    def __init__(self,side):
        self.side = side
        self._prices = []
        self._orders = {}
        self._newTopOfBook = False

    def newTopOfBook(self):
        return self._newTopOfBook

    def getSide(self):
        return "SELL" if self.side == Order.SELL else "BUY"

    def addOrder(self,order):
        '''Agregar una orden al book requiere de varios pasos
        a) si el precio de la orden no está en el book debo agregar el precio a la lista
        de precios existentes y ordenar nuevamente la lista
        b) si el precio ya existe, debo agregar la orden a la lista de prioridades'''
        if not order.price in self._orders:
            logging.info(f"Adding new price {order.price} to {self.getSide()}")
            self._orders[order.price] = []
            self._prices.append(order.price)
            self._prices.sort(reverse=False if self.side == Order.SELL else True)
            #Si el nuevo precio es el mejor, entonces tengo un nuevo top of book
            self._newTopOfBook= self._prices[0] == order.price
        logging.info(f"Adding new order {order.price} size {order.size} to {self.getSide()}")
        self._orders[order.price].append(order)

    def isEmpty(self):
        return len(self._prices) == 0

    def getTopOfBook(self):
        return self._orders[self._prices[0]][0] if not self.isEmpty() else None

    def removeTopOfBook(self):
        tob =  self._orders[self._prices[0]].pop(0)
        logging.info(f"Removing {tob}")
        #Debo verificar si al sacar la orden me queda ese precio vacio
        if len(self._orders[tob.price]) == 0:
            logging.info(f"Removing price {tob.price} status {tob.status} from {self.getSide()} Book")
            self._prices.remove(tob.price)
            del(self._orders[tob.price])

class Trade:

    ID = 0

    def __init__(self,bb,ba):
        Trade.ID +=1
        self.id = Trade.ID
        self.buyAccount = bb.account
        self.sellAccount = ba.account
        #si estaba antes en el book el bid tomo el precio del bid, si etaba el ask tomo el precio del ask
        self.price = bb.price if bb.timestamp < ba.timestamp else ba.price
        self.size = min(bb.size,ba.size)
        self.symbol = bb.symbol
        self.timestamp = ba.timestamp if bb.timestamp < ba.timestamp else bb.timestamp
        bb.updateSize(self.size)
        ba.updateSize(self.size)
        bb._account.updatePosition(self,bb.side)
        ba._account.updatePosition(self,ba.side)

    def __str__(self):
        return f"Trade #{self.id} time {self.timestamp} price {self.price} size {self.size} symbol {self.symbol}"

if __name__ == '__main__':
    import csv

    logging.basicConfig(
        level=logging.DEBUG,
        format='{asctime} {levelname} ({threadName:10s}) {message}',
        style='{'
    )

    engine = MatchingEngine()

    with open("dodic_order_book.csv","r") as orders:
        header = orders.readline().split("\n")[0].split(",")
        reader = csv.DictReader(orders,header)
        for row in reader:
            order = Order(**row)
            engine.addOrder(order)
    engine.showTrades()
    engine.showProfit()
    engine.plotTrades()