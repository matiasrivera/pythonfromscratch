#coding=utf-8

from Class_7.e05_heritage import Gato, Perro
from Class_7.e02_class_and_instance import Animal

class Caballo(Animal):

    def __init__(self):
        self._tipo_animal = 'Caballo'

    def emitir_sonido(self):
        print ("iiiiiiiiiiiiiii")


if __name__== "__main__":
    lista_animales = []

    lista_animales.append(Gato('Maullero'))
    lista_animales.append(Perro('Tom'))
    lista_animales.append(Caballo())

    for animal in lista_animales:
        animal.emitir_sonido()



