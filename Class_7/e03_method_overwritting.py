'''
Here are some examples of special object properties:

__init__: the initialisation method of an object, which is called when the object is created.
__str__: the string representation method of an object, which is called when you use the str function to convert that
        object to a string.
__class__: an attribute which stores the the class (or type) of an object – this is what is returned when you use the
        type function on the object.
__eq__: a method which determines whether this object is equal to another. There are also other methods for determining
        if it’s not equal, less than, etc.. These methods are used in object comparisons, for example when we use the equality
        operator == to check if two objects are equal.
__add__ is a method which allows this object to be added to another object. There are equivalent methods for all the
        other arithmetic operators. Not all objects support all arithemtic operations – numbers have all of these methods
        defined, but other objects may only have a subset.
__iter__: a method which returns an iterator over the object – we will find it on strings, lists and other iterables.
        It is executed when we use the iter function on the object.
__len__: a method which calculates the length of an object – we will find it on sequences. It is executed when we use
        the len function of an object.
__dict__: a dictionary which contains all the instance attributes of an object, with their names as keys. It can be
        useful if we want to iterate over all the attributes of an object. __dict__ does not include any methods, class
        attributes or special default attributes like __class__. '''


class Complejo:

    def __init__(self, real, imaginario):
        self.real = real
        self.imaginario = imaginario

    #Sobrescribo el método que convierte a cadena
    def __str__(self):
        cadena = str(self.real)
        if self.imaginario > 0:
            cadena += '+'
        cadena += (str(self.imaginario) + 'i')
        return cadena


    #metodo que se ejecuta al usar el operador <
    def __lt__(self, other):
        modulo_self = self.calcular_modulo(self)
        modulo_other = self.calcular_modulo(other)
        return modulo_self < modulo_other

    def calcular_modulo(self, complejo):
        return (complejo.real ** 2 + complejo.imaginario ** 2) ** 0.5

    #metodo que se ejecuta al usar el operador >
    def __gt__(self, other):
        modulo_self = self.calcular_modulo(self)
        modulo_other = self.calcular_modulo(other)
        return modulo_self > modulo_other

    #metodo que se ejecuta al usar el operador <=
    def __le__(self, other):
        modulo_self = self.calcular_modulo(self)
        modulo_other = self.calcular_modulo(other)
        return modulo_self <= modulo_other

    #metodo que se ejecuta al usar el operador >=
    def __ge__(self, other):
        modulo_self = self.calcular_modulo(self)
        modulo_other = self.calcular_modulo(other)
        return modulo_self >= modulo_other

    #metodo que se ejecuta al usar el operador ==
    def __eq__(self, other):
        return self.real == other.real and self.imaginario == other.imaginario

    #metodo que se ejecuta al usar el operador !=
    def __ne__(self, other):
        modulo_self = self.calcular_modulo(self)
        modulo_other = self.calcular_modulo(other)
        return modulo_self != modulo_other

    #metodo que se ejecuta al usar el operador +
    def __add__(self, other):
        nuevo = Complejo(self.real + other.real, self.imaginario + other.imaginario)
        return nuevo

if __name__== "__main__":
    complex = Complejo(3, -5)
    print (complex)
    complex2 = Complejo(-8, 2)
    print (complex2)

    complex3 = Complejo(-8, 3)
    print (complex < complex2)

    print (complex == complex2)
    print (complex2 != complex3)

    print (complex + complex2)
    print (complex)


