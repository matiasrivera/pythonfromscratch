'''Escribir el programa del examen 1, pero esta vez intentando detectar los objetos que se necesitan'''


''' Botella  y   Sacacorchos
a)  Escribir  una  clase  Corcho,  que  contenga  un  atributo  bodega  (cadena  con  el  nombre  de  la
bodega).
b)  Escribir  una  clase  Botella  que  contenga  un  atributo  corcho  con  una  referencia  al  corcho  que
la  tapa,  o   None  si  está  destapada.
    c)  Escribir  una  clase  Sacacorchos  que  tenga  un  método  destapar  que  le  reciba  una  botella,  le
saque  el  corcho  y   se  guarde  una  referencia  al  corcho  sacado.  Debe  lanzar  una  excepción  en  el
caso  en  que  la  botella  ya  esté  destapada,  o   si  el  sacacorchos  ya  contiene  un  corcho.
    d)  Agregar  un  método  limpiar,  que  saque  el  corcho  del  sacacorchos,  o   lance  una  excepción  en
el  caso  en  el  que  no  haya  un  corcho.'''



'''  Modelar  una  clase  Mate  que  describa  el  funcionamiento  de  la  conocida  bebida  tradicional
local.  La  clase  debe  contener  como  miembros:
a)  Un  atributo  para  la  cantidad  de  cebadas  restantes  hasta  que  se  lava  el  mate  (representada por  un  número).
    b)  Un  atributo  para  el  estado  (lleno  o   vacío).
    c)  El  constructor  debe  recibir  como  parámetro  n,  la  cantidad  máxima  de  cebadas  en  base  a   la
cantidad  de  yerba  vertida  en  el  recipiente.
    d)  Un  método  cebar,  que  llena  el  mate  con  agua.  Si  se  intenta  cebar  con  el  mate  lleno,  se  debe
lanzar  una  excepción  que  imprima  el  mensaje  Cuidado!  Te  quemaste!"
e)  Un  método  beber,  que  vacía  el  mate  y   le  resta  una  cebada  disponible.  Si  se  intenta  beber  un
mate  vacío,  se  debe  lanzar  una  excepción  que  imprima  el  mensaje  .El  mate  está  vacío!"
f)  Es  posible  seguir  cebando  y   bebiendo  el  mate  aunque  no  haya  cebadas  disponibes.  En  ese
caso  la  cantidad  de  cebadas  restantes  se  mantendrá  en  0,  y   cada  vez  que  se  intente  beber  se
debe  imprimir  un  mensaje  de  aviso:  .Advertencia:  el  mate  está  lavado.",  pero  no  se  debe  lanzar
una  excepción.Herencia  y   Polimorfismo'''


'''  Papel,  Birome,  Marcador
a)  Escribir  una  clase  Papel  que  contenga  un  texto,  un  método  escribir,  que  reciba  una  cadena
para  agregar  al  texto,  y   el  método  __str__  que  imprima  el  contenido  del  texto.
    b)  Escribir  una  clase  Birome  que  contenga  una  cantidad  de  tinta,  y   un  método  escribir,  que
reciba  un  texto  y   un  papel  sobre  el  cual  escribir.  Cada  letra  escrita  debe  reducir  la  cantidad  de
tinta  contenida.  Cuando  la  tinta  se  acabe,  debe  lanzar  una  excepción.
    c)  Escribir  una  clase  Marcador  que  herede  de  Birome,  y   agregue  el  método  recargar,  que
reciba  la  cantidad  de  tinta  a   agregar.'''



''' Juego  de  Rol
a)  Escribir  una  clase  Personaje  que  contenga  los  atributos  vida,  posicion  y   velocidad,  y   los
métodos  recibir_ataque,  que  reduzca  la  vida  según  una  cantidad  recibida  y   lance  una
excepción  si  la  vida  pasa  a   ser  menor  o   igual  que  cero,  y   mover  que  reciba  una  dirección  y   se
mueva  en  esa  dirección  la  cantidad  indicada  por  velocidad.
    b)  Escribir  una  clase  Soldado  que  herede  de  Personaje,  y   agregue  el  atributo  ataque  y   el
método  atacar,  que  reciba  otro  personaje,  al  que  le  debe  hacer  el  daño  indicado  por  el  atributo
ataque.
    c)  Escribir  una  clase  Campesino  que  herede  de  Personaje,  y   agregue  el  atributo  cosecha  y   el
método  cosechar,  que  devuelva  la  cantidad  cosechada.

'''
