'''Python Classes and Objects
Python Classes/Objects
Python is an object oriented programming language.

Almost everything in Python is an object, with its properties and methods.

A Class is like an object constructor, or a "blueprint" for creating objects.

To create a class, use the keyword class:

Example
Create a class named MyClass, with a property named x:'''

class MyClass:
  x = 5

p1 = MyClass()
p2 = MyClass()
p3 = p1
print(p1.x)
p1.x = 10
print(p1.x)
print(p3.x)


'''The __init__() Function
The examples above are classes and objects in their simplest form, and are not really useful in real life applications.

To understand the meaning of classes we have to understand the built-in __init__() function.

All classes have a function called __init__(), which is always executed when the class is being initiated.

Use the __init__() function to assign values to object properties, or other operations that are necessary to do when the object is being created:'''

class Person:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def sayHello(self):
        print("Hello my name is " + self.name)

    def sayGoodbye(self):
        pass

    def sayAge(self):
        print(f"My age is {self.age}")

p1 = Person("John", 36)
p2 = Person("Raffi", 52)

print(p1.name)
print(p1.age)
p1.sayHello()
p1.sayAge()

print(p2.name)
print(p2.age)
p2.sayHello()

