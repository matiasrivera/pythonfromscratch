import random

class Producto:

    def __init__(self,nombre,marca,precio,categoria,codigo,descuento=random.randint(0,30)/100,impuesto=random.choice([0.105,0.21,0.27])):
        self.codigo = codigo
        self.nombre = nombre
        self.marca = marca
        self.categoria = categoria
        self.precio = precio
        self.descuento = descuento
        self.impuesto = impuesto

    def getNombre(self):
        return self.nombre

    def __str__(self):
        return f"{self.getNombre()} {self.marca}"

class Bebida(Producto):

    def __init__(self,marca,nombre,precio,envase,codigo):
        super().__init__(nombre,marca,precio,categoria=self.__class__.__name__,codigo=codigo)
        self.envase = envase


class Fernet(Bebida):

    def __init__(self,marca,precio,nombre="Fernet"):
        super().__init__(marca,nombre,precio,envase="Botella",codigo=random.randint(1,10000000))


class Lacteo(Producto):

    def __init__(self,marca,nombre,precio,codigo):
        super().__init__(nombre,marca,precio,categoria=self.__class__.__name__,codigo=codigo)

class Yogurt(Lacteo):

    def __init__(self,marca,sabor,precio,nombre="Yogurt"):
        super().__init__(marca,nombre,precio,codigo=random.randint(1,10000000))
        self.sabor = sabor

    def getNombre(self):
        return f"{super().getNombre()} {self.sabor}"

class Queso(Lacteo):

    def __init__(self,marca,estilo,precio,nombre="Queso"):
        super().__init__(marca,nombre,precio,codigo=random.randint(1,10000000))
        self.estilo = estilo

    def getNombre(self):
        return f"{super().getNombre()} {self.estilo}"

class Carrito:

    def __init__(self):
        self.datos = []

    def agregarItem(self,itemCarrito):
        self.datos.append(itemCarrito)

    def agregarItems(self,items):
        self.datos.extend(items)

    def getItems(self):
        return self.datos


class ItemCarrito:

    def __init__(self,producto,cantidad):
        self.producto = producto
        self.cantidad = cantidad
        
    def getMontoNeto(self):
        return self.producto.precio*self.cantidad
    
    def getDescuento(self):
        return self.getMontoNeto()*self.producto.descuento

    def getImpuesto(self):
        return self.getMontoNeto()*self.producto.impuesto    
    
    def getMontoBruto(self):
        return self.getMontoNeto()-self.getDescuento()+self.getImpuesto()
    
    def getItemFactura(self):
        return {"nombre":self.producto.nombre,"cantidad":self.cantidad,"neto":self.getMontoNeto(),"descuento":self.getDescuento(),"impuesto":self.getImpuesto(),"subtotal":self.getMontoBruto()}
        

class Almacen:

    def __init__(self):
        self.productos = {}

    def agregarProducto(self,producto):
        if producto.codigo not in self.productos:
            self.productos[producto.codigo] = producto

    def __str__(self):
        return str([str(p.codigo)+":"+p.nombre for p in self.productos.values()])

class Facturador:

    def __init__(self):
        pass

    def facutrarCarrito(self,carrito):
        factura = []
        for item in carrito.getItems():
            factura.append(item.getItemFactura())
        return factura

    def imprimirFactura(self,factura):
        print(f"{'Producto':10}  {'Cantidad'}     {'Neto':8}  {'Descuento'}   {'Impuesto'}    {'Subtotal'}")
        total = 0
        for item in factura:
            total+=item['subtotal']
            print(f"{item['nombre']:10}  {item['cantidad']:7}     ${item['neto']:>8.2f}  ${item['descuento']:>6.2f}     ${item['impuesto']:>6.2f}  ${item['subtotal']:>8.2f}")
        print(f"{'Total':*>52}    ${total:8.2f}")

if __name__== "__main__":

    #yerba = Producto("yerba",100)
    #coca = Producto("coca",80,descuento=0.2)
    #carne = Producto("carne",400,impuesto=0.105)
    queso = Queso("La Serenisima","Muzzarella",500)
    yogurt = Yogurt("Tregar","Frutilla",300)
    fernet = Fernet("Branca",400)
    productos = [queso,yogurt,fernet]
    almacen = Almacen()
    almacen.agregarProducto(queso)
    almacen.agregarProducto(yogurt)
    almacen.agregarProducto(fernet)
    carrito = Carrito()
    facturador = Facturador()

    print(almacen)
    for producto in productos:
        print(producto.getNombre())
        print(producto)
        print(producto.categoria)
        almacen.agregarProducto(producto)

    print(almacen)

    item1 = ItemCarrito(queso,5)
    item2 = ItemCarrito(yogurt,2)
    item3 = ItemCarrito(fernet,3)


    carrito.agregarItems([item1,item2,item3])

    factura = facturador.facutrarCarrito(carrito)
    facturador.imprimirFactura(factura)

