import threading
import time
import queue
import logging
import random
import datetime

logging.basicConfig(
    level=logging.INFO,
    format='{asctime} {levelname} ({threadName:10s}) {message}',
    style='{'
)

def terminar(td=0):
    logging.info("Soy el thread {name} y terminé y tarde {td}".format(name=threading.currentThread().name,td=td))

def trabajar():
    while True:
        seconds = cola.get()
        logging.info("Soy el thread {name} voy a realizar una operación larga de {seconds} segundos".format(name=threading.currentThread().name,seconds=seconds))
        now = datetime.datetime.now()

        time.sleep(seconds)
        terminar(datetime.datetime.now()-now)

cola = queue.Queue()
for x in range(4):
    t1 = threading.Thread(name="T"+str(x),target=trabajar)
    t1.start()


i=1
while True:
    seconds = random.randint(1,20)
    logging.info("Enviando trabajo #{i} de {seconds} segundos".format(i=i,seconds=seconds))
    i+=1
    cola.put(seconds)
    s = random.randint(1,5)
    logging.info("Voy a dormir {seconds} segundos".format(seconds=s))
    logging.info("Hay {pendientes} trabajos pendientes".format(pendientes=cola.qsize()))
    time.sleep(s)
