import threading
import time
import logging
import random
import datetime

logging.basicConfig(
    level=logging.INFO,
    format='{asctime} {levelname} ({threadName:10s}) {message}',
    style='{'
)

def terminar(td=0):
    logging.info("Soy el thread {name} y terminé y tarde {td}".format(name=threading.currentThread().name,td=td))

def esperar(seconds):
    logging.info("Soy el thread {name} voy a realizar una operación larga de {seconds} segundos".format(name=threading.currentThread().name,seconds=seconds))
    now = datetime.datetime.now()
    time.sleep(seconds)
    terminar(datetime.datetime.now()-now)


t1 = threading.Thread(name="T1",target=esperar,args=[random.randint(1,20)])
t2 = threading.Thread(name="T2",target=esperar,args=[random.randint(1,20)])
t3 = threading.Thread(name="T3",target=esperar,args=[random.randint(1,20)])

t1.start()
t2.start()
t3.start()

# esperar(5)
# esperar(10)
# esperar(15)
terminar()