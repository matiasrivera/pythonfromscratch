'''
http://api.primary.com.ar/assets/apidoc/trading/index.html

http://api.primary.com.ar/assets/docs/Primary-API.pdf


'''


import pyRofex

pyRofex.initialize(user="matiasrivera364",
                   password="ePMoRh7@",
                   account="matiasrivera364",
                   environment=pyRofex.Environment.REMARKET)

instrumentList = pyRofex.get_all_instruments()["instruments"]
cficodes = set(instrument["cficode"] for instrument in instrumentList)
print(instrumentList[0])
print(cficodes)

# 2-Get all available segments and print all segment ids
segments = pyRofex.get_segments()

for segment in segments['segments']:
    print("Segment ID: {0}".format(segment['marketSegmentId']))

# Get a list of all instruments and then count the number of instruments return
instruments = pyRofex.get_all_instruments()
print("Number of Instruments: {0}".format(len(instruments['instruments'])))

# 4-Get a detailed list of the instruments and then check the Low Limit Price for the first instrument return
detailed = pyRofex.get_detailed_instruments()["instruments"]
print(detailed[0])
#print("Low Limit Price for {0} is {1}.".format(detailed['instruments'][0]['instrumentId']['symbol'],
#                                               detailed['instruments'][0]['lowLimitPrice']))