import pyRofex
import logging
import queue
import threading

class QueueManager:

    __instance = None

    @staticmethod
    def getInstance():
        """ Static access method. """
        if not QueueManager.__instance:
            QueueManager()
        return QueueManager.__instance

    def __init__(self):
        if not QueueManager.__instance:
            logging.info(f"Creating QueueManager Instance")
            QueueManager.__instance = self
            self.marketdataQueue = queue.Queue()
            self.excecutionReportQueue = queue.Queue()
            self.subscriptionQueue = queue.Queue()

    def subscribeMD(self,data):
        self.subscriptionQueue.put(data)

    def readMDSubscription(self):
        return self.subscriptionQueue.get()

    def sendMarketData(self,msg):
        self.marketdataQueue.put(msg)

    def sendExcecutionReport(self,msg):
        self.excecutionReportQueue.put(msg)

    def readMarketData(self):
        return self.marketdataQueue.get()

    def readExecutionReport(self):
        return self.excecutionReportQueue.get()

class ConnectionHandler:

    def __init__(self,user,password,account=None,environment=pyRofex.Environment.REMARKET):
        pyRofex.initialize(user=user,
                           password=password,
                           account=account if account else user,
                           environment=environment)
        self.queueManager = QueueManager.getInstance()
        pyRofex.init_websocket_connection(market_data_handler=self.queueManager.sendMarketData,
                                      order_report_handler=self.queueManager.sendExcecutionReport,
                                      error_handler=self.onError,
                                      exception_handler=self.onError)
        pyRofex.order_report_subscription(snapshot=False)
        logging.info(f"pyRofex connection successfully established for user {user}")
        subscriptorThread = threading.Thread(name="MDSubscriptor",target=self.subscribeMD)
        subscriptorThread.start()
        logging.info(f"Subsciption Thread Started")

    def onError(self,message):
        logging.error(f"ERROR!!!! {message}")

    def subscribeMD(self):
        while True:
            data = self.queueManager.readMDSubscription()
            tickers,entries = data
            logging.info(f"Received MD subscription for {tickers} {entries}")
            pyRofex.market_data_subscription(tickers,entries)

class Strategy:

    def __init__(self,instrument,operacion="SELL"):
        self.instrument = [instrument]
        self.operacion = pyRofex.Side.SELL if operacion == "SELL" else pyRofex.Side.BUY

        self.side = "BI" if self.operacion == pyRofex.Side.SELL else "OF"

        self.queueManager = QueueManager.getInstance()
        self.queueManager.subscribeMD((self.instrument,[pyRofex.MarketDataEntry.BIDS,pyRofex.MarketDataEntry.LAST,pyRofex.MarketDataEntry.OFFERS]))
        mdThread = threading.Thread(name="ProcessMD",target=self.processMD)
        orThread = threading.Thread(name="ProcessOR",target=self.processOR)
        mdThread.start()
        logging.info("Started MDThread")
        orThread.start()
        logging.info("Started ORThread")

    def processMD(self):
        while True:
            message = self.queueManager.readMarketData()
            logging.info(f"marketdata {message}")
            msg = message["marketData"]
            if len(msg[self.side]) > 0:
                price = msg[self.side][0]["price"]
                size = 5#msg[self.side][0]["size"]
                id = pyRofex.send_order(self.instrument[0],size,pyRofex.OrderType.LIMIT,self.operacion,price=price)
                logging.info(f"clientId {id}")
                #status = pyRofex.get_order_status(id['order']["clientId"],id['order']["proprietary"])
                #logging.info(f"status {status}")

    def processOR(self):
        while True:
            message = self.queueManager.readExecutionReport()
            logging.info(f"orderreport {message}")



if __name__ == "__main__":

    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s %(levelname)s (%(threadName)-10s) %(message)s',
    )

    handler = ConnectionHandler(user="matiasrivera364",password="ePMoRh7@",account="REM363")
    strategy = Strategy("Rfx20/jun21")
    #strategy2 = Strategy("WTI")
