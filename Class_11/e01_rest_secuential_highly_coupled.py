import pyRofex
import logging


logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(levelname)s (%(threadName)-10s) %(message)s',
)

pyRofex.initialize(user="matiasrivera364",
                   password="ePMoRh7@",
                   account="REM364",
                   environment=pyRofex.Environment.REMARKET)

ticker = "Rfx20/Jun21"
entries = [pyRofex.MarketDataEntry.BIDS,pyRofex.MarketDataEntry.LAST,pyRofex.MarketDataEntry.OFFERS]

operacion = pyRofex.Side.SELL

side = "BI" if operacion == pyRofex.Side.SELL else "OF"

message = pyRofex.get_market_data(ticker,entries)
logging.info(f"marketdata {message}")
msg = message["marketData"]
price = msg[side][0]["price"]
size = 10#msg[side][0]["size"]

id = pyRofex.send_order(ticker,size,pyRofex.OrderType.LIMIT,operacion,price=price)
logging.info(f"clientId {id}")
status = pyRofex.get_order_status(id['order']["clientId"],id['order']["proprietary"])
logging.info(f"status {status}")