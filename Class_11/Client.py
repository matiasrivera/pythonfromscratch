from ppi_client.models.account_movements import AccountMovements
from ppi_client.ppi import PPI
from ppi_client.models.balanceandpositions import BalanceAndPositions
from ppi_client.models.product_type import ProductType
from ppi_client.models.orders_filter import OrdersFilter
from ppi_client.models.item import Item
from ppi_client.models.term import Term
from datetime import datetime, timedelta
import threading
import asyncio
import json
import queue

def processResponse(func):
    def wrapper(*params):
        response = func(*params)
        if response.httpStatus == 200 and response.status != 0:
            raise Exception(f"Error with API Call {response.message}")
        return response.payload
    return wrapper

class Security:

    def __init__(self,**kwargs):
        for name,value in kwargs.items():
            self.__setattr__(name, value)

class Client:

    def __init__(self,user,password,processMD):
        self._ppi = PPI(sandbox=True)
        self._securityCache = {}
        self._login(user,password)
        self._socket = threading.Thread(name="Socket",target=self._open_socket,args=[processMD])
        self._socket.start()
        self._queue = queue.Queue()
        self._q = threading.Thread(name="Queue",target=self.ds)
        self._q.start()


    async def dosubscribe(self):
        while True:
            security = self._queue.get()
            print(f"Subscribing {security.ticker}")
            await self._ppi.realtime.subscribe_to_element(Item(security.id, 3), self._token)


    def ds(self):
        loop = asyncio.new_event_loop()
        asyncio.set_event_loop(loop)

        loop.run_until_complete(self.dosubscribe())
        loop.run_forever()

    def subscribe(self,security):
        if type(security) is list:
            for s in security:
                if type(s) is str:
                    security = self.getSecurity(s)
                self._queue.put(security)
        else:
            if type(security) is str:
                security = self.getSecurity(security)
            self._queue.put(security)


    def _login(self,user,password):
        auth_result = self._ppi.auth.auth_login(user, password)
        self._token = auth_result['accessToken']
        self._refresh_token = auth_result['refreshToken']

    def _open_socket(self,processMD):
        self._loop = asyncio.new_event_loop()
        asyncio.set_event_loop(self._loop)
        self._loop.run_until_complete(self._ppi.realtime.connect_websocket(self._token, self._refresh_token, self._refresh, self._connect, self._disconnect, processMD))
        self._loop.run_forever()

    async def _connect(self):
        print("Connecting")

    async def _disconnect(self):
        print("Disconnecting")

    def _refresh(self,data):
        auth_result = self._ppi.auth.refresh_token(self._refresh_token)
        self._refresh_token = auth_result['refreshToken']

    @processResponse
    def _search(self,securityName):
        return self._ppi.marketdata.item_search(securityName, token=self._token)

    @processResponse
    def _getMD(self,security,plazo):
        return self._ppi.marketdata.get_current_marketdata(Item(security.id,plazo),self._token)

    def getSecurity(self,securityName):
        if not securityName in self._securityCache:
            response = self._search(securityName)
            if type(response) is list:
                for instrument in response:
                    self._securityCache[instrument["ticker"]] = Security(**instrument)
        return self._securityCache[securityName]

    def getMarketData(self,security,plazo=3):
        if type(security) is str:
            security = self.getSecurity(security)
        return self._getMD(security,plazo)



if __name__ == '__main__':

    def onmarketdata(data):
        msg = json.loads(data)

        if msg["LastTrade"]:
            print("[%s] Ultimo operado %.2f"%(msg["Ticker"], msg["UltimoOperado"]))
        else:
            print("[%s] Actualizacion ofertas: %.2f - %.2f" % (msg["Ticker"], msg["Bids"][0]["Precio"], msg["Offers"][0]["Precio"]))


    client = Client('usuariodemo11524', '123456789',onmarketdata)
    data = client.getMarketData("AL30",3)
    print(data)
    data = client.getMarketData("GD30",3)
    print(data)
    data = client.getMarketData("GGAL",3)
    print(data)
    data = client.getMarketData("BMA",3)
    print(data)
    client.subscribe("BMA")
    client.subscribe("GGAL")
    client.subscribe("AL30")
    #await client.subscribe("GGAL")