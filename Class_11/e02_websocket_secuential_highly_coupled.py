import pyRofex
import logging


logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s %(levelname)s (%(threadName)-10s) %(message)s',
)

pyRofex.initialize(user="matiasrivera364",
                   password="ePMoRh7@",
                   account="REM364",
                   environment=pyRofex.Environment.REMARKET)

def processMD(message):
    logging.info(f"marketdata {message}")
    msg = message["marketData"]
    if len(msg[side]) > 0:
        price = msg[side][0]["price"]
        size = 10
        id = pyRofex.send_order(ticker[0],size,pyRofex.OrderType.LIMIT,operacion,price=price)
#        logging.info(f"clientId {id}")
#        status = pyRofex.get_order_status(id['order']["clientId"],id['order']["proprietary"])
#        logging.info(f"status {status}")

def processOR(message):
    logging.info(f"orderreport {message}")

def onError(message):
    logging.error(f"error {message}")

ticker = ["Rfx20/jun21"]
entries = [pyRofex.MarketDataEntry.BIDS,pyRofex.MarketDataEntry.LAST,pyRofex.MarketDataEntry.OFFERS]

pyRofex.init_websocket_connection(market_data_handler=processMD,
                                  order_report_handler=processOR,
                                  error_handler=onError,
                                  exception_handler=onError)
logging.info("Conexión exitosa")
pyRofex.market_data_subscription(tickers=ticker,
                                 entries=entries)

pyRofex.order_report_subscription()

operacion = pyRofex.Side.BUY

side = "BI" if operacion == pyRofex.Side.SELL else "OF"


